openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -subj '/O=color Inc./CN=color.az' -keyout ca.color.az.key -out ca.color.az.crt

openssl req -out color.az.csr -newkey rsa:2048 -nodes -keyout color.az.key -subj "/CN=color.az/O=color organization"

openssl x509 -req -days 365 -CA ca.color.az.crt -CAkey ca.color.az.key -set_serial 0 -in color.az.csr -out color.az.crt



